async function getUsers() {
    return fetch("https://randomuser.me/api/?results=50")
        .then(response => {
            return response.json();
        })
        .then(data => {
            return data.results;
        })
        .catch(error => {
            console.error(error);
            return null;
        });
}

const results = await getUsers();

async function loadTeachers() {
    return fetch("http://localhost:3000/posts")
        .then(response => {
            return response.json();
        })
        .catch(error => {
            console.error(error);
            return null;
        });
}

const loaded = await loadTeachers();

function generateRandomColor() {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    return `#${r.toString(16).padStart(2, '0')}${g.toString(16).padStart(2, '0')}${b.toString(16).padStart(2, '0')}`;
}

const courses = ["Mathematics",
    "Physics",
    "English",
    "Computer Science",
    "Dancing",
    "Chess",
    "Biology",
    "Chemistry",
    "Law",
    "Art",
    "Medicine",
    "Statistics",];

function formatTeachersArray(users) {
    function generateRandomId() {
        const min = 10000;
        const max = 99999;
        const randomId = Math.floor(Math.random() * (max - min + 1)) + min;
        return String(randomId);
    }

    return _.map(users, (teacher) => {

        return {
            gender: teacher.gender,
            title: teacher.name.title,
            full_name: teacher.name.first + " " + teacher.name.last,
            city: teacher.location.city,
            state: teacher.location.state,
            country: teacher.location.country,
            postcode: teacher.location.postcode,
            coordinates: {
                latitude: teacher.location.coordinates.latitude,
                longitude: teacher.location.coordinates.longitude
            },
            timezone: {
                offset: teacher.location.timezone.offset,
                description: teacher.location.timezone.description
            },
            email: teacher.email,
            b_date: teacher.dob.date,
            age: teacher.dob.age,
            phone: teacher.phone,
            picture_large: teacher.picture.large,
            picture_thumbnail: teacher.picture.thumbnail,
            id: (!teacher.id.name || !teacher.id.value) ? generateRandomId() : (teacher.id.name + teacher.id.value),
            favorite: false,
            course: _.sample(courses),
            bg_color: generateRandomColor(),
            note: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab autem consectetur culpa cumnque, " +
                "distinctio dolor dolore dolorem doloremque ea explicabo facilis nam nesciunt nisi pariatur perspiciatis " +
                "porro quis similique temporibus veniam veritatis? A ab ad, aliquam amet consequatur cupiditate debitis " +
                "deserunt doloribus dolorum earum eius eos minus nostrum odit omnis perferendis...",
        }

    });

}

function strIsValid(str) {
    return _.isString(str) && str.charAt(0) === str.charAt(0).toUpperCase();
}

function validateTeacher(teacher) {
    const fullName = teacher.full_name.split(" ");
    const isFullNameValid = (strIsValid(fullName[0]) && strIsValid(fullName[1]));
    const isNoteValid = strIsValid(teacher.note);
    const isStateValid = strIsValid(teacher.state);
    const isCityValid = strIsValid(teacher.city);
    const isCountryValid = strIsValid(teacher.country);
    const isAgeValid = _.isNumber(teacher.age);
    const isPhoneValid = /\d/.test(teacher.phone);
    let isEmailValid = true;
    if("email" in teacher) isEmailValid = teacher.email.includes("@");
    const isSubjectValid = strIsValid(teacher.course);
    return (
        isFullNameValid &&
        isNoteValid &&
        isStateValid &&
        isCityValid &&
        isCountryValid &&
        isAgeValid &&
        isPhoneValid &&
        isEmailValid &&
        isSubjectValid
    );
}

function correctStr(str) {
    if(!str) return "";
    str = String(str);
    if(str.charAt(0).match(/[a-z]/)){
        return _.upperFirst(str);
    }else{
        return str;
    }
}

function correctTeacher(teacher){
    const fullName = teacher.full_name.split(" ");
    if(!strIsValid(fullName[0]) && !strIsValid(fullName[1])){
        teacher.full_name = correctStr(fullName[0]) + " " + correctStr(fullName[1]);
    }
    if(!strIsValid(teacher.note)){
        teacher.note = correctStr(teacher.note);
    }
    if(!strIsValid(teacher.state)){
        teacher.state = correctStr(teacher.state);
    }
    if(!strIsValid(teacher.city)){
        teacher.city = correctStr(teacher.city);
    }
    if(!strIsValid(teacher.country)) {
        teacher.country = correctStr(teacher.country);
    }
    if(!strIsValid(teacher.course)) {
        teacher.course = correctStr(teacher.course);
    }
    return teacher;
}

function filterTeachers(teachers, filters) {
    return _.filter(teachers, (teacher) => {
        return _.every(filters, (value, key) => {
            if (key === "photo") {
                return !value || "picture_large" in teacher;
            } else if (key === "age") {
                const ageRange = value.split("-");
                const min = parseInt(ageRange[0]);
                const max = parseInt(ageRange[1]);
                return teacher.age >= min && teacher.age <= max;
            } else {
                return teacher[key] === value;
            }
        });
    });
}


function sortTeachers(teachers, key, order) {
    const direction = order ? "asc" : "desc";
    return _.orderBy(teachers, key, direction);
}
function findTeacherByParam(teachers, param) {
    return _.find(teachers, (teacher) => {
        if (!/^\d+$/.test(param)) {
            if(teacher.note!=null){
                return (teacher.full_name.toLowerCase().includes(param.toLowerCase()) || teacher.note.toLowerCase().includes(param.toLowerCase()));
            }
        } else {
            return teacher.age == param;
        }
        return false;
    });
}

function percentOfTeachers(teachers, param, value){
    const filtered = teachers.filter((teacher) => {
        if(param === "age" && "age" in teacher){
            const ageRange = value.split("-");
            const min = ageRange[0];
            const max = ageRange[1];
            if(teacher.age < min || teacher.age > max){
                return false;
            }
        }
        if(param === "country" && "country" in teacher){
            if(teacher.country !== value){
                return false;
            }
        }
        return true;
    });
    const length = filtered.length;
    return (length*100)/teachers.length;
}

const teachers = formatTeachersArray(results).concat(loaded);
console.log(teachers);
const uniqueCountries = new Set();

teachers.forEach(teacher => {
    uniqueCountries.add(teacher.country);
});

const filterCountries = document.getElementById("region");
const popupCountries = document.getElementById("country-input");
updateCountries();
updateAgeFilter();
let filteredTeachers;
let statisticTeachers = sortTeachers(teachers, "full_name", true);
let favTeachers;
const teachersSection = document.querySelector(".top-teachers");
const carousel = document.querySelector(".carousel");
const filtersSection = document.querySelector(".filter");
filtersSection.addEventListener("change", updateFilteredTeachers);
updateFilteredTeachers();
const main = document.querySelector(".main-container");
const searchButton = document.querySelector(".info button");
const searchInput = document.querySelector(".info input");

searchButton.addEventListener("click", showTeacher);
searchInput.addEventListener('keypress', function (event) {
    if (event.key === 'Enter') {
        showTeacher();
    }
});
teachersSection.addEventListener("click", function(event) {
    let clickedTeacher = event.target.closest(".photo-wrapper");
    if(!clickedTeacher) return;
    infoPopup.style.display = "flex";
    main.style.filter = "blur(5px)";
    let teacherDiv = clickedTeacher.closest(".teacher");
    let fullName = teacherDiv.querySelector(".teacher-name-label").textContent + " " + teacherDiv.querySelector(".teacher-surname-label").textContent;
    let neededTeacher = findTeacherByParam(filteredTeachers, fullName);
    changeInfoPopup(neededTeacher);
});

const infoPopup = document.getElementById("teacher-info-popup");
const starButton = infoPopup.querySelector(".star-button");
starButton.addEventListener("click", makeFav);
let teacherCard = infoPopup.querySelector(".teacher-card");
const mapDetails = teacherCard.querySelector(".teacher-card-map");
infoPopup.querySelector(".close-button").addEventListener("click", function (){
    if(mapDetails.hasAttribute("open")) mapDetails.removeAttribute("open");
    mapDetails.style.display = "";
    infoPopup.style.display = "none";
    main.style.filter = "";
});

function updateAgeFilter() {
    const ageFilter = document.getElementById("age");
    let minAge = _.minBy(teachers, "age").age;
    let maxAge = _.maxBy(teachers, "age").age;
    let midAge = Math.floor(_.mean([minAge, maxAge]));
    ageFilter.querySelectorAll("option")[1].textContent = minAge + "-" + midAge;
    ageFilter.querySelectorAll("option")[2].textContent = (midAge+1) + "-" + maxAge;
}


function updateCountries() {
    filterCountries.innerHTML = "";
    popupCountries.innerHTML = "";
    let filterFragment = document.createDocumentFragment();
    let popupFragment = document.createDocumentFragment();
    let allOption = document.createElement("option");
    allOption.textContent = "All";
    filterFragment.appendChild(allOption);
    for(const country of uniqueCountries){
        let option = document.createElement("option");
        option.textContent = country;
        filterFragment.appendChild(option);
        option = document.createElement("option");
        option.textContent = country;
        popupFragment.appendChild(option);
    }
    filterCountries.appendChild(filterFragment);
    popupCountries.appendChild(popupFragment);
}

function updateFilteredTeachers(){
    teachersSection.innerHTML = "";
    let filters = {
        photo: false
    };
    let ageInput = filtersSection.querySelector("#age");
    let countryInput = filtersSection.querySelector("#region");
    let genderInput = filtersSection.querySelector("#sex");
    if(ageInput.value!=="All")filters.age = ageInput.value;
    if(countryInput.value!=="All") filters.country = countryInput.value;
    if(genderInput.value!=="All") filters.gender = genderInput.value.toLowerCase();
    if(filtersSection.querySelector("#only-photo").checked){
        filters.photo = true;
    }
    if(filtersSection.querySelector("#only-fav").checked){
        filters.favorite = true;
    }
    filteredTeachers = filterTeachers(teachers, filters);
    createTeachers(filteredTeachers, true);
}

function createTeachers(array, param) {
    let fragment = document.createDocumentFragment();
    for (const teacher of array) {
        let teacherDiv = document.createElement("div");
        teacherDiv.classList.add("teacher");
        if(param){
            if(teacher.favorite === true) teacherDiv.classList.add("fav");
        }
        let photoWrapper = document.createElement("div");
        photoWrapper.classList.add("photo-wrapper");
        let fullName = teacher.full_name.split(" ");
        if(teacher.picture_large) {
            let image = document.createElement("img");
            image.classList.add("teacher-photo");
            image.setAttribute("src", teacher.picture_large);
            photoWrapper.appendChild(image);
        }else {
            let initials = document.createElement("span");
            initials.classList.add("teacher-initials");
            initials.textContent = fullName[0].charAt(0)+"."+fullName[1].charAt(0);
            photoWrapper.appendChild(initials);
        }
        teacherDiv.appendChild(photoWrapper);
        let teacherInfo = document.createElement("div");
        teacherInfo.classList.add("teacher-info");
        let teacherNameDiv = document.createElement("div");
        teacherNameDiv.classList.add("teacher-name");
        let teacherName = document.createElement("span");
        teacherName.classList.add("teacher-name-label");
        teacherName.textContent = fullName[0];
        teacherNameDiv.appendChild(teacherName);
        let teacherSurname = document.createElement("span");
        teacherSurname.classList.add("teacher-surname-label");
        teacherSurname.textContent = fullName[1];
        teacherNameDiv.appendChild(teacherSurname);
        teacherInfo.appendChild(teacherNameDiv);
        if(param) {
            let subject = document.createElement("span");
            subject.classList.add("teacher-subject");
            subject.textContent = teacher.course;
            teacherInfo.appendChild(subject);
        }
        let region = document.createElement("span");
        region.classList.add("teacher-region");
        region.textContent = teacher.country;
        teacherInfo.appendChild(region);
        teacherDiv.appendChild(teacherInfo);
        fragment.appendChild(teacherDiv);
    }
    if(param){
        teachersSection.appendChild(fragment);
    }else{
        carousel.appendChild(fragment);
    }
}

let map;
function changeInfoPopup(teacher) {
    let image = teacherCard.querySelector(".teacher-card-photo");
    if("picture_large" in teacher) {
        image.style.display = "block";
        image.setAttribute("src", teacher.picture_large);
    }else {
        image.style.display = "none";
    }
    let name = teacherCard.querySelector(".teacher-card-name");
    name.textContent = teacher.full_name;
    let subject = teacherCard.querySelector(".teacher-card-subject");
    subject.textContent = teacher.course;
    let region = teacherCard.querySelector(".teacher-card-region");
    region.textContent = teacher.city + ", " + teacher.country;
    let ageSex = teacherCard.querySelector(".teacher-card-age-sex");
    if("age" in teacher) {
        ageSex.textContent = teacher.age + ", " + correctStr(teacher.gender);
    }else {
        ageSex.textContent = correctStr(teacher.gender);
    }
    let email = teacherCard.querySelector(".teacher-card-email");
    email.textContent = teacher.email;
    let phone = teacherCard.querySelector(".teacher-card-tel");
    phone.textContent = teacher.phone;
    let note = teacherCard.querySelector("p");
    note.textContent = teacher.note;
    let days = teacherCard.querySelector(".days-to-birthday");
    days.textContent = daysToBirthday(teacher);
    if("coordinates" in teacher) {
        mapDetails.style.display = "block";
        let latitude = parseFloat(teacher.coordinates.latitude);
        let longitude = parseFloat(teacher.coordinates.longitude);
        if (map) {
            map.remove();
        }
        map = L.map('teacher-map').setView([latitude, longitude], 13);
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19
        }).addTo(map);
        L.marker([latitude, longitude]).addTo(map)
            .bindPopup('<b>' + teacher.full_name + '</b><br>' + teacher.city)
            .openPopup();
    }
    updateStarButton(teacher);
}

function daysToBirthday(teacher) {
    const today = dayjs();
    let nextBirthday = dayjs(teacher.b_date).year(today.year());
    if (today.isAfter(nextBirthday)) {
        nextBirthday = nextBirthday.add(1, 'year');
    }
    return nextBirthday.diff(today, 'day');
}

function updateStarButton(teacher) {
    if(teacher.favorite) {
        starButton.style.backgroundImage = "url(\"/src/images/star-full.png\")";
    }else {
        starButton.style.backgroundImage = "url(\"/src/images/star-empty.png\")";
    }
}

function makeFav() {
    let teacher = findTeacherByParam(teachers, infoPopup.querySelector(".teacher-card-name").textContent);
    teacher.favorite = !teacher.favorite;
    updateStarButton(teacher);
    updateFilteredTeachers();
    updateFavList();
}

function showTeacher() {
    if(searchInput.value.replace(/\s/g, "").length){
        let teacher = findTeacherByParam(teachers, searchInput.value);
        if(teacher){
            infoPopup.style.display = "flex";
            changeInfoPopup(teacher);
        }
    }
    searchInput.value = "";
}

function updateFavList() {
    carousel.innerHTML = "";
    let filter = {favorite:true};
    favTeachers = filterTeachers(teachers, filter);
    createTeachers(favTeachers, false);
    updateSlider();
}


const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");
const carouselWrapper = document.querySelector(".carousel-wrapper");
let pos = 0;
let count = 5;
let width;
leftArrow.addEventListener("click", function (){
    width = carouselWrapper.offsetWidth/count;
    pos += width*count;
    pos = Math.min(pos, 0);
    carousel.style.transform = `translateX(${pos}px)`;
    updateArrows();
});

rightArrow.addEventListener("click", function() {
    if (favTeachers.length > count) {
        width = carouselWrapper.offsetWidth / 5;
        pos -= width * count;
        pos = Math.max(pos, -width * (favTeachers.length - count));
        carousel.style.transform = `translateX(${pos}px)`;
        updateArrows();
    }
});

function updateArrows(){
    if(pos === 0) leftArrow.classList.add("hidden");
    else leftArrow.classList.remove("hidden");
    if((pos === - width * (favTeachers.length - count)) || favTeachers.length<=5) rightArrow.classList.add("hidden");
    else rightArrow.classList.remove("hidden");
}

function updateSlider() {
    pos = 0;
    updateArrows();
    carousel.style.transform = `translateX(0px)`;
}

const table = document.querySelector("table");
const tableHead = table.querySelector("thead");
const tableButtons = document.querySelector(".table-buttons");
const moreButton = tableButtons.querySelector(".button-more");
const lastButton = tableButtons.querySelector(".last-button");
const backButton = tableButtons.querySelector(".back-button");
let clickedPageButton;
let tablePage = 1;
let sortValue = "full_name";
let sortOrder = true;
let numberOfButtons = Math.ceil(teachers.length/10);
createTableButtons();
updateTable(tablePage);
tableHead.addEventListener("click", function(event){
    let clickedTd = event.target.closest("th");
    let value = clickedTd.classList[0];
    sortValue = value;
    clickedTd.classList.toggle("in-order");
    clickedTd.querySelector(".table-hover").classList.toggle("reversed");
    let order = clickedTd.classList.contains("in-order");
    sortOrder = order;
    statisticTeachers = sortTeachers(teachers, value, order);
    updateTable(tablePage);
});

moreButton.addEventListener("click", updateHiddenButtons);
backButton.addEventListener("click", updateHiddenButtons);
lastButton.addEventListener("click", function () {
    clickedPageButton.classList.remove("active");
    clickedPageButton = lastButton;
    lastButton.classList.add("active");
    let buttons = tableButtons.querySelectorAll("button");
    let activeButton =  buttons[buttons.length-4];
    activeButton.classList.add("active");
    updateTable(Math.ceil(teachers.length/10));
});

tableButtons.addEventListener("click", function(event){
    let clickedPage = event.target.closest(".page-number");
    if(!clickedPage) return;
    clickedPageButton.classList.remove("active");
    clickedPageButton = clickedPage;
    clickedPage.classList.add("active");
    let buttons = tableButtons.querySelectorAll("button");
    if(clickedPage === buttons[buttons.length - 4]) lastButton.classList.add("active");
    if(clickedPage!==buttons[buttons.length - 4]){
        buttons[buttons.length - 4].classList.remove("active");
        lastButton.classList.remove("active");
    }
    tablePage = clickedPage.textContent;
    updateTable(tablePage);
});

function updateHiddenButtons() {
    let buttons = tableButtons.querySelectorAll("button");
    for(let i= 3; i < (buttons.length-3); i++) {
        buttons[i].classList.toggle("hidden");
    }
    moreButton.classList.toggle("hidden");
    lastButton.classList.toggle("hidden");
    backButton.classList.toggle("hidden");
}

function updateTable(page) {
    const rows = table.rows;
    for(let i = 1; i < rows.length; i++){
        const cells = rows[i].cells;
        for(let j = 0; j < cells.length; j++){
            cells[j].textContent = "";
        }
    }
    let formula = (page-1)*10;
    for (let i = 0; i < 10; i++){
        const row = table.rows[i+1];
        if(statisticTeachers[formula+i]){
            row.cells[0].textContent = statisticTeachers[formula+i].full_name;
            row.cells[1].textContent = statisticTeachers[formula+i].course;
            if("age" in statisticTeachers[formula+i]){
                row.cells[2].textContent = statisticTeachers[formula+i].age;
            }
            row.cells[3].textContent = correctStr(statisticTeachers[formula+i].gender);
            row.cells[4].textContent = statisticTeachers[formula+i].country;
        }
    }
}

function createTableButtons() {
    let fragment = document.createDocumentFragment();
    for(let i = 1; i<=numberOfButtons; i++) {
        let button = document.createElement("button");
        button.textContent = i;
        button.classList.add("page-number");
        if(i === 1){
            button.classList.add("active");
            clickedPageButton = button;
        }
        if(i>3) button.classList.add("hidden");
        fragment.appendChild(button);
    }
    tableButtons.insertBefore(fragment, moreButton);
}

function updateTableButtons() {
    let newNumber = Math.ceil(teachers.length/10);
    if(newNumber > numberOfButtons) {
        let button = document.createElement("button");
        button.textContent = newNumber;
        button.classList.add("page-number");
        if(newNumber>3) button.classList.add("hidden");
        tableButtons.insertBefore(button, moreButton);
    }
}

const addTeacherButtons = document.querySelectorAll(".add-teacher-button");
const addTeacherPopup = document.getElementById("add-teacher-popup");
const form = addTeacherPopup.querySelector("form");
addTeacherPopup.querySelector(".close-button").addEventListener("click", function(){
   addTeacherPopup.style.display = "none";
    main.removeAttribute("style");
});
addTeacherButtons.forEach((button) => {
    button.addEventListener("click", showAddTeacherPopup);
});

function showAddTeacherPopup() {
    main.style.filter = "blur(5px)";
    addTeacherPopup.style.display = "flex";
}

form.addEventListener("submit", function (event){
   event.preventDefault();
   let teacher = addTeacher();
   teachers.push(teacher);
   form.reset();
   updateFilteredTeachers();
   statisticTeachers = sortTeachers(teachers, sortValue, sortOrder);
   updateTable(tablePage);
   updateTableButtons();

   fetch("http://localhost:3000/posts", {
       method: "POST",
       headers: {
           "Content-Type" : "application/json"
       },
       body: JSON.stringify(teacher)
   })
   .then(response => response.json())
   .catch(error => {
       console.error(error);
   });

});

function addTeacher() {
    let teacher = {};
    if(form.querySelector("#sex-female").checked){
        teacher.gender = "female";
    }else{
        teacher.gender = "male";
    }
    teacher.full_name = form.fullName.value;
    teacher.city = form.city.value;
    teacher.country = form.country.value;
    teacher.email = form.email.value;
    let date = form.date.value.split("-").map(Number);
    let newDate = new Date(date[0], date[1]-1, date[2]+1);
    teacher.b_date = newDate.toISOString();
    teacher.age = calculateAge(newDate);
    teacher.phone = form.phone.value;
    teacher.favorite = false;
    teacher.course = form.speciality.value;
    teacher.bg_color = form.color.value;
    teacher.note = form.note.value;
    if(!validateTeacher(teacher)) teacher = correctTeacher(teacher);
    return teacher;
}

function calculateAge(date) {
    const today = dayjs();
    return today.diff(dayjs(date), "year");
}

function getNumberOfCourse(course) {
    return _.filter(teachers, { 'course': course }).length;
}

courses.sort((a, b) => {return getNumberOfCourse(b) - getNumberOfCourse(a)});

const courseValues = _.map(courses, (course) => {
    return getNumberOfCourse(course);
})

const pieColors = ["#e34234", "#e86c62", "#f6928a", "#fab7b2", "#ffddda"];

new Chart("my-chart", {
    type: 'pie',
    data: {
        labels: courses,
        datasets: [{
            label: "Number of teachers",
            backgroundColor: pieColors,
            data: courseValues,
        }]
    }
});